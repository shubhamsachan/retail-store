package commons;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("constants")
public class ConstantsTest {

  @DisplayName("Product Type Constants")
  @Test
  public void productTypetest() {
    Assertions.assertTrue(Constants.ProductType.GROCERIES.getType().equals("Groceries"));
    Assertions.assertTrue(Constants.ProductType.OTHER.getType().equals("Other"));
  }

  @DisplayName("User Type test")
  @Test
  public void UserTypeTest() {
    Assertions.assertTrue(Constants.UserType.CUSTOMER.getType().equals("Customer"));
    Assertions.assertTrue(Constants.UserType.EMPLOYEE.getType().equals("Employee"));
    Assertions.assertTrue(Constants.UserType.AFFILIATE.getType().equals("Affiliate"));
  }
}
