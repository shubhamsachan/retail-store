package service;

import commons.Constants;
import model.BillModel;
import model.UserModel;
import org.junit.jupiter.api.*;
import org.mockito.Mock;

import java.time.LocalDate;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BillingPredicateTest {

  @Mock
  UserModel userModel;

  @Mock
  BillModel billModel;


  @BeforeAll
  void setModels() {
    userModel = new UserModel(1, "Shubham", "address", LocalDate.now(), "Employee");
    billModel = new BillModel();
  }

  @Test
  public void isUserEmployeeTest() {
    userModel.setUserType(Constants.UserType.EMPLOYEE.getType());
    Assertions.assertTrue(BillingPredicate.isUserEmployee().test(userModel));
  }

  @Test
  public void isUserAffiliateTest() {
    Assertions.assertFalse(BillingPredicate.isUserAffiliate().test(userModel));
    userModel.setUserType(Constants.UserType.AFFILIATE.getType());
    Assertions.assertTrue(BillingPredicate.isUserAffiliate().test(userModel));
  }

  @Test
  public void isUserCustomer() {
    Assertions.assertFalse(BillingPredicate.isUserCustomer().test(userModel));
    userModel.setUserType(Constants.UserType.CUSTOMER.getType());
    Assertions.assertTrue(BillingPredicate.isUserCustomer().test(userModel));
  }

  @Test
  public void isCustomerTwoYrsOld() {
    Assertions.assertFalse(BillingPredicate.isCustomerTwoYrsOld().test(userModel));
    userModel.setCreatedAt(LocalDate.of(2014, 01, 14));
    Assertions.assertTrue(BillingPredicate.isCustomerTwoYrsOld().test(userModel));
  }

  @Test
  public void isBillMoreThan100Test() {
    billModel.setBillAmt(1000);
    System.out.println(billModel);
    Assertions.assertTrue(BillingPredicate.isBillMoreThan100().test(billModel));
  }
}
