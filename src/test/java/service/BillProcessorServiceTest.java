package service;

import model.BillModel;
import model.ProductModel;
import model.UserModel;
import org.junit.jupiter.api.*;
import static org.mockito.Mockito.*;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.Arrays;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BillProcessorServiceTest {

  @Test
  void discountOnBillAmtTest() {
    BillModel billModel = new BillModel();
    billModel.setBillAmt(100);
    BillProcessorService billProcessorService = new BillProcessorService();
    Assertions.assertEquals(billProcessorService.discountOnBillAmt(billModel), 5.0);
    billModel.setBillAmt(1000);
    Assertions.assertEquals(billProcessorService.discountOnBillAmt(billModel), 50.0);
    billModel.setBillAmt(990);
    Assertions.assertEquals(billProcessorService.discountOnBillAmt(billModel), 45.0);
  }

  @Test
  void discountOnBillEmployeeTest() {
    BillModel billModel = new BillModel(1,
      Arrays.asList(new ProductModel(1, "Rice", "Groceries", 50.0), new ProductModel(1, "Wheat", "Groceries", 50.0), new ProductModel(1, "Product1", "Other", 150.0), new ProductModel(1, "Product2", "Other", 250.0)),
      new UserModel(1, "Shubh", "Mumbai", LocalDate.of(2014, 01, 01), "Employee"),
      LocalDate.of(2019, 9, 30),
      500.0
      );
    BillProcessorService billProcessorService = new BillProcessorService();
    Assertions.assertEquals(billProcessorService.discountOnBill(billModel), 120.0);
  }

  @Test
  void discountOnBillAffiliateTest() {
    BillModel billModel = new BillModel(1,
      Arrays.asList(new ProductModel(1, "Rice", "Groceries", 50.0), new ProductModel(1, "Wheat", "Groceries", 50.0), new ProductModel(1, "Product1", "Other", 150.0), new ProductModel(1, "Product2", "Other", 250.0)),
      new UserModel(1, "Shubh", "Mumbai", LocalDate.of(2014, 01, 01), "Affiliate"),
      LocalDate.of(2019, 9, 30),
      500.0
    );
    BillProcessorService billProcessorService = new BillProcessorService();
    Assertions.assertEquals(billProcessorService.discountOnBill(billModel), 40.0);
  }

  @Test
  void discountOnBillCustomerTest() {
    BillModel billModel = new BillModel(1,
      Arrays.asList(new ProductModel(1, "Rice", "Groceries", 50.0), new ProductModel(1, "Wheat", "Groceries", 50.0), new ProductModel(1, "Product1", "Other", 150.0), new ProductModel(1, "Product2", "Other", 250.0)),
      new UserModel(1, "Shubh", "Mumbai", LocalDate.of(2014, 01, 01), "Customer"),
      LocalDate.of(2019, 9, 30),
      500.0
    );
    BillProcessorService billProcessorService = new BillProcessorService();
    Assertions.assertEquals(billProcessorService.discountOnBill(billModel), 20.0);
  }

  @Test
  void billProcessorTest() {
    BillModel billModel = new BillModel(1,
      Arrays.asList(new ProductModel(1, "Rice", "Groceries", 50.0), new ProductModel(1, "Wheat", "Groceries", 50.0), new ProductModel(1, "Product1", "Other", 150.0), new ProductModel(1, "Product2", "Other", 250.0)),
      new UserModel(1, "Shubh", "Mumbai", LocalDate.of(2014, 01, 01), "Customer"),
      LocalDate.of(2019, 9, 30),
      500.0
    );
    BillProcessorService billProcessorService = new BillProcessorService();
    Assertions.assertEquals(billProcessorService.billProcessor(billModel), 455.0);
  }
}
