package service;

import commons.Constants;
import model.BillModel;

public class BillProcessorService {

  public double billProcessor(final BillModel billModel) {
    double billAmt = billModel.getBillAmt();
    return billAmt - discountOnBillAmt(billModel) - discountOnBill(billModel);
  }

  /**
   * For every $100 on the bill, there would be a $ 5 discount (e.g. for $ 990, you get $ 45
   *  as a discount).
   * @param billModel bill received
   * @return discounted amount
   */
  double discountOnBillAmt(final BillModel billModel) {
    if (BillingPredicate.isBillMoreThan100().test(billModel)) {
      return ((int) (billModel.getBillAmt() / Constants.BILL_AMT_100)) * Constants.BILL_AMT_DISCOUNT;
    }
    return 0.0;
  }

  /**
   * If the user is an employee of the store, he gets a 30% discount
   * If the user is an affiliate of the store, he gets a 10% discount
   * If the user has been a customer for over 2 years, he gets a 5% discount.
   * The percentage based discounts do not apply on groceries.
   * A user can get only one of the percentage based discounts on a bill.
   * @param billModel received bill
   * @return discounted amount
   */
  double discountOnBill(final BillModel billModel) {
    double amount = billModel.getProductModelList()
                              .stream()
                              .filter(BillingPredicate.isProductNotGrocery())
                              .map(obj -> obj.getPrice())
                              .reduce((a,b) -> a + b).orElse(0.0);

    if (BillingPredicate.isUserEmployee().test(billModel.getUser())) {
      return  (amount * Constants.EMPLOYEE_DISCOUNT)/100;
    } else if (BillingPredicate.isUserAffiliate().test(billModel.getUser())) {
      return  (amount * Constants.AFFILIATE_DISCOUNT)/100;
    } else if (BillingPredicate.isUserCustomer().test(billModel.getUser()) &&
      BillingPredicate.isCustomerTwoYrsOld().test(billModel.getUser())) {
      return (amount * Constants.CUSTOMER_DISCOUNT)/100;
    }
    return 0.0;
  }
}
