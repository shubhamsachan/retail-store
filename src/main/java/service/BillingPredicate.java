package service;

import commons.Constants;
import model.BillModel;
import model.ProductModel;
import model.UserModel;

import java.time.LocalDate;
import java.time.Period;
import java.util.function.Predicate;

public class BillingPredicate {

  /**
   * Check if the user type is employee
   * @return boolean
   */
  public static Predicate<UserModel> isUserEmployee() {
    return p -> p.getUserType().equals(Constants.UserType.EMPLOYEE.getType());
  }

  /**
   * Check if the user type is affiliate
   * @return boolean
   */
  public static Predicate<UserModel> isUserAffiliate() {
    return p -> p.getUserType().equals(Constants.UserType.AFFILIATE.getType());
  }

  /**
   * Check if the user type is customer
   * @return boolean
   */
  public static Predicate<UserModel> isUserCustomer() {
    return p -> p.getUserType().equals(Constants.UserType.CUSTOMER.getType());
  }

  /**
   * Check if the customer is two years old or not
   * @return boolean
   */
  public static Predicate<UserModel> isCustomerTwoYrsOld() {
    return p -> Period.between(p.getCreatedAt(), LocalDate.now()).getYears() >= 2 ;
  }

  /**
   * Check if the bill amount is more than 100.00 rs or not
   * @return boolean
   */
  public static Predicate<BillModel> isBillMoreThan100() {
    return p -> p.getBillAmt() >= Constants.BILL_AMT_100;
  }

  /**
   * checks if the product type is not grocery
   * @return true if not grocery
   */
  public static Predicate<ProductModel> isProductNotGrocery() {
    return p -> !p.getType().equals(Constants.ProductType.GROCERIES.getType());
  }
}
