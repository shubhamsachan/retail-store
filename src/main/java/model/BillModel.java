package model;

import java.time.LocalDate;
import java.util.List;

/**
 * Model class for bill
 */
public class BillModel {
  private int id;
  private List<ProductModel> productModelList;
  private UserModel user;
  private LocalDate billDate;
  private double billAmt;

  public BillModel(int id, List<ProductModel> productModelList, UserModel user, LocalDate billDate, double billAmt) {
    this.id = id;
    this.productModelList = productModelList;
    this.user = user;
    this.billDate = billDate;
    this.billAmt = billAmt;
  }

  public BillModel() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public List<ProductModel> getProductModelList() {
    return productModelList;
  }

  public void setProductModelList(List<ProductModel> productModelList) {
    this.productModelList = productModelList;
  }

  public UserModel getUser() {
    return user;
  }

  public void setUser(UserModel user) {
    this.user = user;
  }

  public LocalDate getBillDate() {
    return billDate;
  }

  public void setBillDate(LocalDate billDate) {
    this.billDate = billDate;
  }

  public double getBillAmt() {
    return billAmt;
  }

  public void setBillAmt(double billAmt) {
    this.billAmt = billAmt;
  }

  @Override
  public String toString() {
    return "BillModel{" +
      "id=" + id +
      ", productModelList=" + productModelList +
      ", user=" + user +
      ", billDate=" + billDate +
      ", billAmt=" + billAmt +
      '}';
  }
}

