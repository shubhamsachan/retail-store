package model;

import java.time.LocalDate;
import java.util.Date;

/**
 * model class for user
 */
public class UserModel {
  private int id;
  private String name;
  private String address;
  private LocalDate createdAt;
  private String userType;

  public UserModel(int id, String name, String address, LocalDate createdAt, String userType) {
    this.id = id;
    this.name = name;
    this.address = address;
    this.createdAt = createdAt;
    this.userType = userType;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public LocalDate getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDate createdAt) {
    this.createdAt = createdAt;
  }

  public String getUserType() {
    return userType;
  }

  public void setUserType(String userType) {
    this.userType = userType;
  }

  @Override
  public String toString() {
    return "UserModel{" +
      "id=" + id +
      ", name='" + name + '\'' +
      ", address='" + address + '\'' +
      ", createdAt=" + createdAt +
      ", userType='" + userType + '\'' +
      '}';
  }
}
