package commons;

public class Constants {

  public static final double BILL_AMT_100 = 100.0;
  public static final double EMPLOYEE_DISCOUNT = 30;
  public static final double AFFILIATE_DISCOUNT = 10;
  public static final double CUSTOMER_DISCOUNT = 5;
  public static final double BILL_AMT_DISCOUNT = 5;

  public enum ProductType {
    GROCERIES("Groceries"), OTHER("Other");
    String type;

    ProductType(String type) {
      this.type = type;
    }

    public String getType() {
      return type;
    }
  }

  public enum UserType {
    CUSTOMER("Customer"), EMPLOYEE("Employee"), AFFILIATE("Affiliate");
    String type;

    UserType(String type) {
      this.type = type;
    }

    public String getType() {
      return type;
    }
  }
}
