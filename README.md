1- BillProcessorService class has billProcessor method which takes a BillModel as an argument to calculate final amount.

title __SERVICE's Class Diagram__\n
  namespace  {
    namespace service {
      class service.BillProcessorService {
          + billProcessor()
          ~ discountOnBill()
          ~ discountOnBillAmt()
      }
    }
  }
  

  namespace  {
    namespace service {
      class service.BillingPredicate {
          {static} + isBillMoreThan100()
          {static} + isCustomerTwoYrsOld()
          {static} + isProductNotGrocery()
          {static} + isUserAffiliate()
          {static} + isUserCustomer()
          {static} + isUserEmployee()
      }
    }
  }
  
  2-  All the conditions are placed inside BillingPredicate.
 